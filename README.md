SSIS
=====

These are the SSIS packages used by Jurisdiction Online and related products. Current SSIS coding standards are located on the wiki here: [SSIS Standards](https://bitbucket.org/praeses/jo-coding-style-guidelines/wiki/SSIS%20Standards)

Development
----------------------

Anytime a new package is created, a dtsConfig file should be created for it so that we can use Octopus variable substitution during the deployment. A list of SSIS Octopus variables can be found here: [SSIS Octopus Variables](https://octopus.jurisdictiononline.com/app#/library/variables/LibraryVariableSets-21). Octopus variable substitution uses the following syntax for variable substitution: #{VariableName}.

Once the new package has been created and the dtsConfig is in place, there are two things that have to be done in order for Octopus to pick up the new package and deploy it.
1. In the c:\dev\ssis\build directory, edit the Deploy-SSIS.ps1 script and add the new package and path to the hash table.
2. Both the JO Misc and JO Misc Dev projects in Octopus have to have the Deploy SSIS steps updated to look for the new config.
    1. In Octopus, open the project (e.g. JO MISC Dev)
    2. Select 'Process' in the left hand menu
    3. Click the 'Deploy SSIS' step
    4. Under 'Substitute variables in files' add the new target path to 'Target files' 
    5. Save the step

Config Files
----------------------

SSIS package configuration files .dtsConfig should be located under *Projects\Configs\PackageGroupFolder\PackageName.dtsConfig* in git.
On local dev environments and database servers, the configuration files will be deployed to *C:\configs\SSIS\PackageGroupFolder\PackageName.dtsConfig*

SSIS XSD schema files should be located under the *Projects\XSD* folder. They will be deployed to *C:\configs\SSIS\XSD\*.

SSIS template files should be located under the *Projects\Templates* folder. They will be deployed to *C:\configs\SSIS\Templates\*. Templates should always be checked into git for tracking.

Deployment
----------------------

To deploy all packages manually, switch to the build folder and run the build.bat file. Then copy the build\package\SSIS folder to the target database server and run the Deploy.ps1 script.

Otherwise, you can point the [SSIS Dev Build](https://teamcity.jurisdictiononline.com/viewType.html?buildTypeId=JurisdictionOnlineDeveloperBuilds_SsisDev) at your dev branch and build, then use the JO Misc Dev (Dev & UAT) and JO Misc (RC & Prod) deployments in Octopus to deploy it to any database server.

**NOTE: If deploying to the dev db server, all JOL connection strings are set to use the JOL_Integration db by default.**
