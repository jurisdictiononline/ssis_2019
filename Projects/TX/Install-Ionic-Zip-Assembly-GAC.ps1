#Requires -RunAsAdministrator

[System.Reflection.Assembly]::Load("System.EnterpriseServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a") | Out-Null
$publish = New-Object System.EnterpriseServices.Internal.Publish            

Write-Output "This script will install Ionic.Zip.dll into the GAC"

$path = Resolve-Path .
$filename = [System.IO.Path]::Combine($path, "Ionic.Zip.dll");
$publish.GacInstall("$filename")
Write-Output "Installed $filename into GAC"
