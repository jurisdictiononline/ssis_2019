#Requires -RunAsAdministrator

[System.Reflection.Assembly]::Load("System.EnterpriseServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a") | Out-Null
$publish = New-Object System.EnterpriseServices.Internal.Publish            

Write-Output "This script will install Microsoft.SqlServer.XE.Core.dll into the GAC"
# Microsoft.SqlServer.XEvent.Linq 12.0 is already install in the GAC, but Microsoft.SqlServer.XE.Core is a dependancy that is _not_
# This is not an issue with SQL Server 2016, as both assemblies are installed in the GAC out of the box.

$paths = (
    "C:\Program Files\Microsoft SQL Server\120\Shared\", # Location on server
    "C:\Program Files (x86)\Microsoft SQL Server\120\Tools\Binn\ManagementStudio\Extensions\Application\" # Location on workstation
);
foreach ($path in $paths)
{
    $filename = [System.IO.Path]::Combine($path, "Microsoft.SqlServer.XE.Core.dll");
    if ([System.IO.File]::Exists($filename))
    {
        $publish.GacInstall($filename)
        Write-Output "Installed $filename into GAC"
    }
}

Write-Output "Verifying we can load from GAC, you should see output below"
[System.Reflection.Assembly]::Load("Microsoft.SqlServer.XE.Core, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91")
