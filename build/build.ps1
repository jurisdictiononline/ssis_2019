# required parameters :
#       $buildNumber

Framework "4.0"

properties {
	# build properties
    $projectName = "SSIS"
    $projectConfig = "Release"
    $baseDir = resolve-path .\..		# ..\Batch Programs - RU26
    $sourceDir = "$baseDir"				# ..\Batch Programs - RU26
    $buildDir = "$baseDir\build"		# ..\Batch Programs - RU26\build
    $packageDir = "$buildDir\package"	# ..\Batch Programs - RU26\build\package
    $version = $buildNumber

	if (!$nuGetSuffix)
    {
        $nuGetSuffix = "Dev"
    }

	# tools
    $nuget = "$baseDir\tools\NuGet.exe"

	# source location
    $SSISSourceDir = "$sourceDir\Projects"

    # package location
    $SSISPackageDir = "$packageDir\SSIS"

	# nuspec files
    $SSISNuspec = "$SSISPackageDir\SSIS.nuspec"

	# deploy scripts
    $SSISDeployFile = "$buildDir\Deploy-SSIS.ps1"
}

task default -depends PackageNuGet

# Initialize the build, delete any existing package folder
task Init {
    Write-Host "Deleting the package directory"
    DeleteFile $packageDir
}

# Copy the necessary files, all the SSIS, config, & template files
task PackageSSIS -depends Init {
	Write-Host "Packaging SSIS"
	$dir = "$packageDir\SSIS"
    CreateDirectory $SSISPackageDir

    $include =  @('*.dtsx', '*.dtsConfig', '*.xls', '*.xlsx', '*.txt', '*.xml', '*.xsd', '*.mdb');
    Write-Host "Copying $SSISSourceDir to $SSISPackageDir\content\"
    CopyFilesPreserveFolders "$SSISSourceDir\" "$SSISPackageDir\content\" "" $include 
    
    Write-Host "Copying $SSISDeployFile to $SSISPackageDir\Deploy.ps1"
    cp $SSISDeployFile "$SSISPackageDir\Deploy.ps1"

    attrib -r "$SSISPackageDir\*.*" /S /D
}

task PackageNuGet -depends PackageSSIS {
	Write-Host "Create SSIS nuget manifest"
    CreateNuGetManifest $version "SSIS" $SSISNuspec "SSIS Packages" "These are the SSIS packages to be deployed"

	Write-Host "exec { & $nuget pack $SSISNuspec -BasePath $SSISPackageDir -OutputDirectory $packageDir }"
    exec { & $nuget pack $SSISNuspec -BasePath $SSISPackageDir -OutputDirectory $packageDir -Verbosity quiet }	
}

# Deploy the SSIS locally when build.ps1 is called with this task name
task DeploySSIS -depends PackageSSIS {
	cd $SSISPackageDir 
    & ".\Deploy.ps1"
    cd $baseDir
}

# ------------------------------------------------------------------------------------#
# Utility methods
# ------------------------------------------------------------------------------------#

# Copy files to a destination. Create the directory if it does not exist
function global:CopyFiles($source, $destination, $exclude = @(), $include = @() ){   
	if (!(Test-Path -Path $destination))
	{
		CreateDirectory $destination
	}     
    Get-ChildItem $source -Recurse -Exclude $exclude -Include $include| Copy-Item -Destination $destination -Force
}

function global:CopyFilesPreserveFolders($source, $destination, $exclude = @(), $include = @() ){   
	if (!(Test-Path -Path $destination))
	{
		CreateDirectory $destination
    }     

    Get-ChildItem -Path $source -Recurse -Exclude $exclude -Include $include | 
        Copy-Item -Destination {
            if ($_.PSIsContainer) {
                Join-Path $destination $_.Parent.FullName.Substring($source.length)
                #if (!(Test-Path -Path $dst)) {
                #    CreateDirectory $dst
                #}
                #Write-Host "Folder: $dst"
                #$dst
            } else {
                $dst = Join-Path $destination $_.FullName.Substring($source.length)
                $dstFolder = [System.IO.Path]::GetDirectoryName($dst)
                if (!(Test-Path -Path $dstFolder)) {
                    CreateDirectory $dstFolder
                }
                #Write-Host "File: $dst"
                $dst
            }
        } -Force -Exclude $exclude -Include $include
}

# Create a directory
function global:CreateDirectory($directory_name)
{
    mkdir $directory_name -ErrorAction SilentlyContinue | Out-Null
}

# Create a NuGet manifest file
function global:CreateNuGetManifest($version, $applicationName, $filename, $title, $description)
{
"<?xml version=""1.0""?>
<package xmlns=""http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd"">
  <metadata>
    <id>Praeses.$applicationName.$nuGetSuffix</id>
    <title>$title</title>
    <version>$version</version>
    <authors>Praeses</authors>
    <owners>Praeses</owners>
    <licenseUrl>http://solutions.praeses.com</licenseUrl>
    <projectUrl>http://solutions.praeses.com</projectUrl>
    <iconUrl>http://solutions.praeses.com</iconUrl>
    <requireLicenseAcceptance>false</requireLicenseAcceptance>
    <description>$description</description>
    <summary>$description</summary>
    <language>en-US</language>
  </metadata>
</package>"  | Out-File $filename -encoding "ASCII"
}

# Delete a file if it exists
function global:DeleteFile($file) {
    if ($file)
    {
        Remove-Item $file -force -recurse -ErrorAction SilentlyContinue | Out-Null
    }
}