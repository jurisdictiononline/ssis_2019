$TargetServer = [System.Net.Dns]::GetHostByName($env:computerName).HostName # Default to deploying to local machine, unless on developer workstation
$ProjectFolder = "$pwd\content"
$ConfigFolder = "C:\config\SSIS"

# Check if we are running on an Aptean workstation
if ($TargetServer -like "APT01-*.SWG.CDC.ROOT")
{
    Write-Host "Deployment script is running on Aptean workstation, defaulting to DGA1JODEVDBSVR for DB Server"
    $TargetServer = "DGA1JODEVDBSVR.SWG.CDC.ROOT"
}

if ($OctopusParameters) # will only be true when executed by Octopus Deploy
{
    $TargetServer = $OctopusParameters["DBServer"]
}

Write-Host "Deploying to $TargetServer"

function Copy-SSIS-Config {
    Param ([string] $ProjectFolder, [string] $ConfigFolder)

    $SourceConfigFolder = Join-Path $ProjectFolder "Configs"
    if (!(Test-Path -Path $SourceConfigFolder)) {
        return "SSIS configurations not present, not deploying"
    }
	
    if (!(Test-Path -Path $ConfigFolder)) {
        # Create the config folder (happens on initial server deployment)
        mkdir -Force $ConfigFolder | Out-Null
    }

    Write-Host "Deploying SSIS config files $SourceConfigFolder"
    Copy-Item -Recurse -Force -Path "$SourceConfigFolder\*" -Destination "$ConfigFolder"

    $XSDFolder = Join-Path $ProjectFolder "XSD"
    Write-Host "Deploying SSIS XSD files $XSDFolder"
    Copy-Item -Recurse -Force -Path "$XSDFolder" -Destination "$ConfigFolder"

    $TemplatesFolder = Join-Path $ProjectFolder "Templates"
    Write-Host "Deploying SSIS Templates files $TemplatesFolder"
    Copy-Item -Recurse -Force -Path "$TemplatesFolder" -Destination "$ConfigFolder"
}

function New-SSIS-Folder {
<#
  .SYNOPSIS
  Create a new folder for SSIS on the target SQL Server.
#>
    Param ([string] $TargetServer, [string] $Folder)
    & dtutil /Quiet /FExists "SQL;$Folder" /SourceServer $TargetServer
    if ($LASTEXITCODE -ne 0) {
        & dtutil /Quiet /FCreate "SQL;\;$Folder" /SourceServer $TargetServer
    }
}

function Publish-SSIS-Package {
<#
  .SYNOPSIS
  Publishes a SSIS package on the local filesystem to the target SQL Server instance.
#>
    Param ([string] $TargetServer, [string] $File, [string] $TargetPath)
        & dtutil /Quiet /File $File /Copy "SQL;$TargetPath" /DestServer $TargetServer
        if ($LASTEXITCODE -ne 0) {
            Write-Error "Failed to deploy package $TargetPath. Error Code: $LASTEXITCODE"
        }
}

function Publish-SSIS-Package-Folder {
<#
  .SYNOPSIS
  Publishes all the SSIS packages in a local folder to the target SQL Server instance.
#>
    Param ([string] $TargetServer, [string] $LocalFolder, [string] $TargetFolder)
    New-SSIS-Folder -TargetServer $TargetServer -Folder $TargetFolder
    foreach ($File in Get-ChildItem "$LocalFolder\*.dtsx") {
        $PackageName = [io.path]::GetFileNameWithoutExtension($File)
        Write-Host "Deploying $PackageName"
        Publish-SSIS-Package -TargetServer $TargetServer -File $File.FullName -TargetPath "$TargetFolder\$PackageName"
    }
}

# This is a Hashtable of Target Folder (SQL Server folder name) keys and Local Folder (git) values
$Packages = @{
    "Arise" = "$ProjectFolder\Arise\Arise";
    "Baseline" = "$ProjectFolder\Baseline";
    "BOA" = "$ProjectFolder\BOA\BOA";
    "Chubb" = "$ProjectFolder\Chubb";
    "Data Warehousing" = "$ProjectFolder\Data Warehousing";
    "Export" = "$ProjectFolder\ExportDB";
    "FM" = "$ProjectFolder\FM\FM";
    "FOIA" = "$ProjectFolder\FOIA Imports\FOIA Imports";
    "Import" = "$ProjectFolder\Import\Import";
    "Interchange" = "$ProjectFolder\Interchange\Interchange";
    "JOL" = "$ProjectFolder\JOL\JOL";
    "JOSLM" = "$ProjectFolder\JOSLM\JOSLM";
    "JOSubscription" = "$ProjectFolder\JOSubscription\JOSubscription";
    "KS" = "$ProjectFolder\KS\KS";
    "KY" = "$ProjectFolder\KY\KY";
    "Location Feed" = "$ProjectFolder\Location Feed\Location Feed";
    "Location Matching" = "$ProjectFolder\Location Matching\Location Matching";
    "Maintenance" = "$ProjectFolder\Maintenance";
    "NB Violations" = "$ProjectFolder\NB Violations\NB Violations";
    "NJ Scantron Results Feed" = "$ProjectFolder\NJ Scantron Results Feed\NJ Scantron Results Feed\NJ Scantron Results Feed";
    "TX" = "$ProjectFolder\TX";
    "Territory Updates" = "$ProjectFolder\Territory Updates\Territory Updates";
    "Travelers" = "$ProjectFolder\Travelers\Travelers_InventoryExport\Travelers_InventoryExport";
    "Violation Updates" = "$ProjectFolder\Violation Updates";
    "Zurich" = "$ProjectFolder\Zurich\Zurich";
}

$Packages.GetEnumerator() | ForEach-Object { 
    Publish-SSIS-Package-Folder -TargetServer $TargetServer -TargetFolder $_.Key -LocalFolder $_.Value
} 

Copy-SSIS-Config -ProjectFolder $ProjectFolder -ConfigFolder $ConfigFolder
